\babel@toc {english}{}
\babel@toc {english}{}
\babel@toc {ngerman}{}
\babel@toc {english}{}
\babel@toc {english}{}
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}%
\contentsline {section}{\numberline {1.1}Motivation}{1}{section.1.1}%
\contentsline {section}{\numberline {1.2}Previous Work}{1}{section.1.2}%
\contentsline {section}{\numberline {1.3}Aim of the Work}{2}{section.1.3}%
\contentsline {section}{\numberline {1.4}Structure of Document}{2}{section.1.4}%
\contentsline {chapter}{\numberline {2}Experiment equipment and materials}{4}{chapter.2}%
\contentsline {section}{\numberline {2.1}AvaChute Setup}{4}{section.2.1}%
\contentsline {section}{\numberline {2.2}Data Acquisition}{6}{section.2.2}%
\contentsline {section}{\numberline {2.3}Chute Lightning}{7}{section.2.3}%
\contentsline {section}{\numberline {2.4}Granulate Inlet and Granular Material}{8}{section.2.4}%
\contentsline {section}{\numberline {2.5}Reference Objects}{8}{section.2.5}%
\contentsline {chapter}{\numberline {3}Methods and Experiments}{10}{chapter.3}%
\contentsline {section}{\numberline {3.1}Point Laser Measurements}{10}{section.3.1}%
\contentsline {section}{\numberline {3.2}Optical Speed Measurement Sensors}{11}{section.3.2}%
\contentsline {section}{\numberline {3.3}Camera and Image Calibration}{11}{section.3.3}%
\contentsline {section}{\numberline {3.4}Optical Flow (OF)}{16}{section.3.4}%
\contentsline {section}{\numberline {3.5}Structured Light Illumination (SLI)}{17}{section.3.5}%
\contentsline {section}{\numberline {3.6}Data Storage}{19}{section.3.6}%
\contentsline {section}{\numberline {3.7}Experiment Settings}{19}{section.3.7}%
\contentsline {chapter}{\numberline {4}Results}{21}{chapter.4}%
\contentsline {section}{\numberline {4.1}OSM signals}{21}{section.4.1}%
\contentsline {section}{\numberline {4.2}Optical Flow Parameter Study}{24}{section.4.2}%
\contentsline {section}{\numberline {4.3}Velocity Boundary}{25}{section.4.3}%
\contentsline {section}{\numberline {4.4}Velocity Field}{26}{section.4.4}%
\contentsline {section}{\numberline {4.5}Depth Measurements}{29}{section.4.5}%
\contentsline {section}{\numberline {4.6}Chute Oscillations}{34}{section.4.6}%
\contentsline {chapter}{\numberline {5}Discussion}{36}{chapter.5}%
\contentsline {section}{\numberline {5.1}Limitations}{36}{section.5.1}%
\contentsline {section}{\numberline {5.2}Velocity Profile}{38}{section.5.2}%
\contentsline {section}{\numberline {5.3}Depth Calculation}{38}{section.5.3}%
\contentsline {section}{\numberline {5.4}Granulate Inlet}{39}{section.5.4}%
\contentsline {chapter}{\numberline {6}Conclusion and Outlook}{40}{chapter.6}%
\contentsline {chapter}{\nonumberline Bibliography}{VIII}{chapter*.43}%
\contentsline {chapter}{List of Figures}{X}{chapter*.44}%
\contentsline {chapter}{List of Tables}{XI}{chapter*.45}%
\contentsline {chapter}{\nonumberline List of Symbols}{XII}{chapter*.46}%
\contentsline {chapter}{\nonumberline Abbreviations}{XIV}{chapter*.47}%
\contentsline {chapter}{\numberline {A}Image sequence of Experiment}{XV}{appendix.A}%
\contentsline {chapter}{\numberline {B}Structured Light Illumination Code}{XVI}{appendix.B}%
\contentsline {chapter}{\numberline {C}Optical Flow Code}{XX}{appendix.C}%
\contentsline {chapter}{\numberline {D}HDF file storage Code}{XXIII}{appendix.D}%
