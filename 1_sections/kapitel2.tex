\chapter{Experiment equipment and materials}
\label{sec:setupava}

\section{AvaChute Setup}
\label{sec:setup}

The redesigned chute is named AvaChute and is shown in \autoref{fig:avachutecomp} whereas the used components are marked with red boxes and listed in \autoref{tab:sensors}. This chute has two segments the upper chute segment with a length of \SI{2.5}{\meter} and the lower chute segment with a length of \SI{2}{\meter} whereas both segments have a width of \SI{1}{\meter}. The inclination of both chute segments can be varied using an electrical cylinder. This enables to set the inclination range for the lower chute segment ($\alpha$) from \SI{27}{\degree} to \SI{33}{\degree} and for the upper chute segment ($\beta$) from \SI{34}{\degree} to \SI{40}{\degree}.
 These limitations are only restricted to the electrical cylinder, basically the strut profiles can be adjusted for each segment to the desired inclination limited by the room height. The dimensions of the chute are shown in \autoref{fig:avachuteDimens}.

\begin{figure}[H]
\centering	
\includegraphics[width=1\linewidth]{3_figures/AvaChute.pdf}
\caption[Avalanche Chute setup]{Picture of the experimental setup with components 1-9, that are listed in \autoref{tab:sensors}.}
\label{fig:avachutecomp}
\end{figure}

\begin{table}[H]
 \centering
 \caption[Measurement system components]{Measurement system components}
 \begin{tabular}{c|cc}
 \toprule
  Nr & Components & Type\\ 
 \midrule
 1	& Camera 			& Z-Cam E2\\
 1  & Camera lens       & Panasonic Lumix G Vario \SI{7}{}-\SI{14}{\milli\meter}\\
 2	& LED Strobe light	& Stairville xBrick Quad $16\cdot \SI{8}{\watt}$ RGBW\\
 3a 	& Point Laser 2		& Balluff BOD 63M-LA04-S115\\
     3b 	& Point Laser 1		& Balluff BOD 63M-LA04-S115\\
 4 	& Line Laser Pos 1	& LLMiHybrid 12mm \SI{20}{\milli\watt} / \SI{100}{\milli\watt}\\
 5 	& Granulate inlet	& Spiral duct d\SI{450}{\milli\meter} \SI{45}{\degree} / Flap: checker plate with\\ & &   electrical door lock BEFO 11211MB \\
 6	& Line Laser Pos 2	& LLMiHybrid 12mm \SI{20}{\milli\watt}\\
 7  & OSM 1             & Developed by \cite{GerritGunther}          \\
 8  & OSM 2             & Developed by \cite{GerritGunther}           \\
 9 	& Control Box		& HENSEL 6547 \SI{30}{\centi\meter}*\SI{30}{\centi\meter}*\SI{17}{\centi\meter}			\\ 
 	
 \bottomrule 
 \end{tabular}
 \label{tab:sensors}
 \end{table}

The test material is stored in the granulate inlet (Nr. 5 in \autoref{fig:avachutecomp}), the opening mechanism contains an electrical door lock which is switched by the USB-Relay DLP-IOR4.\newline
All electrical components are connected to the control box mounted to the wall and the interior is shown in \autoref{fig:setupcontrolBox}. The electrical components that are assembled in the control box are listed in \autoref{tab:setupControlBox}. Inside this box the terminal block of the DAQ, the relays and the voltage supply are placed. This box is further connected to the DAQ card and to USB ports of the control PC. A custom Python program is made to control the sensors as well as for data acquisition and post-processing. Using a single program brings the advantage, that the measurement system, means camera, granulate inlet and DAQ (Point lasers, OSMs) are able to start nearly synchronous and have the same timestamp. 

\begin{table}[H]
\centering
\caption[Control Box components]{Control Box components}
\begin{tabular}{c|ccc}
\toprule
Nr	& 	Component & Type & Function	\\
\midrule
1	& Voltage Supply 	&  	ELG-150-12A & Voltage Supply for camera and\\ &	& 	& release mechanism for granulate inlet\\
2	& Voltage Supply 	& 	ELG-150-12A & Voltage Supply for LED Stripes,\\
& & &  OSMs and Point Lasers\\
3	& LED Strip Controller & 	FUT044 & LED Stripe Controller\\
4 	& Terminal Block 	& NI-CB-68LPR  & Connects Sensors to DAQ\\
5 	& USB Relay 		& DLP-IOR4		& Triggers release mechanism\\
& & &  for granulate inlet\\

\bottomrule 
\end{tabular}
\label{tab:setupControlBox}
\end{table}

\begin{figure}[H]
\centering
\includegraphics[width=1\textwidth]{3_figures/AvaChuteDraft.pdf}
\caption{Schematic drawing of the chute with the relevant dimensions.}
\label{fig:avachuteDimens}
\end{figure}


\begin{figure}[H]
\centering
\includegraphics[width=0.5\textwidth, trim = {8cm 0cm 6cm 1cm},clip]{3_figures/ControlBox.pdf}
\caption{Control box where the voltage supply and sensor connection is wired.}
\label{fig:setupcontrolBox}
\end{figure}

\section{Data Acquisition}
\label{sec:daq}

The analogue to digital conversion of the OSMs and point laser is performed with the NI-DAQ 6024e data acquisition card, which is placed in a 32-bit PC with a Windows operating system. The NI-DAQ 6024e is an internal PCI card in the PC  and the terminal block NI-CB-68LPR (Nr. 4 in \autoref{fig:setupcontrolBox}) is used and placed in the control box where all peripherals are connected. 
The maximum sampling rate of the DAQ card is \SI{200}{\kilo\sample\per\second} or \SI{}{\kilo\hertz} for a single channel, but since the DAQ card is multiplexed the more sensors are recorded the lower is the sampling rate. With all sensors of the chute connected, the sample rate drops to \SI{20}{\kilo\hertz}. All sensors are measured as Referenced-Single-Ended (RSE). The measuring range of the DAQ is set to $\pm\SI{5}{\volt}$. \newline
The sampled data of all sensors are stored into a single HDF5-file for further processing which will be addressed later on.


\section{Chute Lightning}
\label{sec:lightning}

The lightning of the chute is done with LED Strobe lights that are mounted left and right above the chute to minimize shadows. The LED modules can be controlled with a software (Q Light Controller Plus \cite{QLCp}) that allows to set all RGB channels with specific timings. The main methods, optical flow and structured light illumination, require bright and dark light, respectively, a pulsed or alternating lightning scheme is used and shown in \autoref{fig:timingScheme}. Here the upper waveform shows the pulsed light, with $T_{L-on}$ as the time of the bright phase (state $l_{on}$) and $T_{L-off}$ as the time of the dark phase (state $L_{off}$), whereas the bottom waveform indicates the shutter times of the video camera. $SH_o$ indicates the shutter opened whereas $SH_c$ refers to a closed shutter over time $t$. All videos are recorded with a frame rate $fps$ of 160 frames per second.

\begin{figure}[H]
\centering
\includegraphics[width=0.9\textwidth]{3_figures/PulsedLight.pdf}
\caption{Pulsed Light timing scheme}
\label{fig:timingScheme}
\end{figure}

The video frames of the bright and dark phase are automatically separated by observing some reference pixels in the image. During the bright phase the OF is applied and in the dark phase the SLI with line laser is extracted. Since the ZCam uses a rolling shutter some frames are not usable when the shutter is partly closed or opened during a change of the lightning. These frames are excluded from further analysis. The light pulses for the experiments are set that the time for the bright phase $T_{L-on}$ is \SI{210}{\milli\second} and for the dark phase $T_{L-off}$ is \SI{40}{\milli\second}. This is the standard setting for the individual experiments performed in \autoref{sec:results}.

\section{Granulate Inlet and Granular Material}
\label{sec:granulate}


The need of a synchronous measurement system is necessary for the AvaChute. To meet this requirement the granulate inlet received a redesign. The inlet of the new design comes up with a bent tube with a diameter of \SI{450}{\milli\meter}. The opening of the outlet flap is secured by an electronic door lock. When the USB Relay of the door lock is triggered, the gate opens due to a preloaded spring and let the granulate flow. Basically, the door lock relays, the camera and the DAQ recording are triggered at the same time. With the automated inlet the time shift of the camera to the DAQ is brought to a minimum. This concept is evaluated by saving the time when the camera command and the DAQ measuring command is sent by the code, which will be discussed later on. In the previous setup of the chute, the inlet was triggered by manually pulling a rope that caused mechanical oscillations of the whole chute. The new automatic granulate inlet reduces these oscillations to a minimum.

The used granulate material for the experiments is Neopor (graphite coated plastic) particles (diameter \SI{1.3}{}-\SI{6.5}{\milli\meter}) mixed with glass spheres (diameter \SI{1.5}{}-\SI{2}{\milli\meter}) and is visualized in \autoref{fig:granulate} \cite{IEEEexample:berti}. Furthermore a volume of approximately \SI{4}{\litre} or \SI{0,004}{\cubic\meter} is used for all experiments. However, the granulate inlet has a volume of about \SI{0,1}{\cubic\meter} allowing for much larger flows in future experiments.

\begin{figure}[H]
\centering
\includegraphics[width=0.7\textwidth]{3_figures/Granulate.jpg}
\caption{Granular media which is used for the experiments.}
\label{fig:granulate}
\end{figure}

\section{Reference Objects}
\label{sec:refObjects}

The SLI algorithm is calibrated and tested with reference objects. Three rectangular boxes are used as reference objects and their properties are listed in \autoref{tab:refObjects}. Notably, the surface texture and reflectivity are relevant factors for the algorithm to automatically extract the faint red laser line on the objects. Box nr. 1 is black with a slightly glossy surface that resemble best the used granulate material. Box nr. 2 is from the surface very similar to the current chute background. The surface and colour of Box nr. 3 resembles the alternative granulate made out of wood.

\begin{table}[H]
 \centering
 \caption[Reference Objects]{Reference Objects}
 \begin{tabular}{c|ccc}
 \toprule
 Nr  & Colour & Surface texture & Depth / cm\\ 
 \midrule
 1  		& Black     & Glossy smooth & 9.5\\
 2        	& White     & Glossy smooth & 5.5\\
 3        	& Brown     & Matte rough         & 4.5\\
 	
 \bottomrule 
 \end{tabular}
 \label{tab:refObjects}
 \end{table}

\begin{figure}[H]
\centering
\includegraphics[width=0.5\textwidth, trim={7.5cm 0 5cm 0}, clip]{3_figures/ReferenceBoxes.png}
\caption[Reference objects for SLI]{Three reference objects are used to evaluate the SLI algorithm. Object 1 is a black box, Object 2 is a white box and the Object 3 is a cardboard box. Detailed description can be found in \autoref{tab:refObjects}.}
\label{fig:measTree}
\end{figure}


A general goal of laser safety regulations is that the smallest necessary laser power should be used. The AvaChute in its current development stage isn't laser safe yet, e.g. no special boundaries are build neither exists some warning lights that are mounted on the entrance door of the laboratory. However, to attain knowledge on how much laser power is needed for the SLI two lasers with different output power are used. One with \SI{20}{\milli\watt} and one with \SI{100}{\milli\watt}. With both lasers several experiments are carried out to get an idea about the reflection on the objects and the granulate. 





