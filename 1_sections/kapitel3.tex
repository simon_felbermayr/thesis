\chapter{Methods and Experiments}
\label{sec:Methods}

The point laser sensors and optical speed sensors of the original chute setup which have been used in \cite{IEEEexample:berti}, \cite{Gleirscher2014} and \cite{bertiPaper} are kept in the new chute setup as reference. In contrary, the control and evaluation of these sensors is carried out by a new software. In addition, new methods to measure the flow depth and flow velocity are developed. The old and new measurement systems can be distinguished into two main groups: the flow depth and the flow velocity measurement. Further both are including point and spatial measurements. This is visualized in \autoref{fig:measTree}. Here the sensors of the previous chute setup are marked with a dashed box, which are the point measurements. All measurement systems and their working principles are discussed in the following sections. Additionally, the data storage and the experiment settings are discussed at the end of this chapter.

\begin{figure}[H]
\centering
\includegraphics[width=0.8\textwidth]{3_figures/MeasurementTree.pdf}
\caption{Measurement System AvaChute}
\label{fig:measTree}
\end{figure}

\section{Point Laser Measurements}
\label{sec:methballuffs}
Flow depth measurements were conducted by using a point laser sensor with the designation Balluff BOD 63M-LA04-S115. These sensors work by the principle of Time-of-Flight (TOF). The conversion factor $\psi$ from voltage to depth of the sensors are gained by taking reference objects to the chute and calculate a conversion factor which is then applied to the analog signal of the laser sensor. This factor is calculated to be \SI{0.02}{\volt\per\centi\meter}. Means for the depth

\begin{equation}
d(t) = \frac{u(t)}{\psi}cos(\epsilon) \,,
\end{equation}

where $u$ is the measured voltage at time $t$. The point laser housing is placed \SI{1}{\meter} above the the lower chute segment surface, whereas the laser point is focused on the upper chute. This causes a specific angle of incidence $\epsilon$. The angle has to be taken into account when calculating the depth of measured objects as shown in above equation.

\section{Optical Speed Measurement Sensors}
\label{sec:methOSM}

The Optical Speed Measurement (OSM) sensors are developed by \cite{GerritGunther}. One of the sensors is shown in \autoref{fig:OSM}. A LED Diode is emitting Ultraviolet (UV) -light that is reflected by a passing object and sensed by a photo diode. Enabling the measurement of velocities two pairs of UV-LED and photo diode are placed in one OSM named channel A and channel B. Both voltage responses of the channels are cross correlated to find the time difference and infer the associated velocity \cite{GerritGunther}. Once both signals are measured the evaluation of the velocity is performed in Python. Both OSM sensors are placed on the upper chute part along the centre line, the exact down-slope positions are shown in \autoref{fig:avachutecomp} and \autoref{fig:avachuteDimens}.

\begin{figure}[H]
\makebox[\linewidth][c]{
\begin{subfigure}[c]{0.5\textwidth}
\includegraphics[width=\textwidth,trim = {8cm 2cm 3cm 1cm},clip]{3_figures/DSC05260.JPG}
\subcaption{OSM sensor housing: The front of the aluminum tube is  covered with glass and the cable inlet is at the back.}
\label{fig:OSMhousing}
\end{subfigure}
\begin{subfigure}[c]{0.455\textwidth}
\includegraphics[width=\textwidth, trim = {12cm 4cm 7cm 4cm},clip]{3_figures/OSM_inside.pdf}
\subcaption{Electronics inside the housing of the OSM. Each channel, A and B, consist of an UV-LED and a photo diode.}
\label{fig:OSMinside}
\end{subfigure}}
\caption[Overview of OSM sensor]{OSM sensor housing and the internal electronics.}
\label{fig:OSM}
\end{figure}

%\begin{figure}[H]
%\centering	
%\includegraphics[width=0.4\textwidth,trim = {8cm 2cm 3cm 1cm},clip]{3_figures/DSC05260.JPG}
%\caption{OSM sensor}
%\label{fig:OSM}
%\end{figure}

\section{Camera and Image Calibration}
\label{sec:methCalibrateImages}
The camera images have two distortions that need corrections prior to any optical image analysis. First distortion of the image are caused by the camera and the camera lens with its position. The distortions are mathematically described by the intrinsic and extrinsic matrices \cite{Zhang2000}. Second distortion is caused by the geometrical arrangement of the camera in respect to the AvaChute. Note that the relative position of the camera in respect to the chute surface does not change when the whole chute is tilted as the camera rotates in the same reference frame. However, if both angles, $\alpha$ and $\beta$, of the lower and upper chute parts are modified relative to each other, this distortion field needs to get revalued.

A mathematical model for generating the intrinsic and extrinsic matrices is required and therefore the pinhole model is used \cite{Zhang2000}. Any point $\mathbf{M}=\begin{bmatrix}x& y& z\end{bmatrix}^{T}$ from the 3D world coordinate system is depicted on the camera as a point $\mathbf{m} = \begin{bmatrix}X & Y
\end{bmatrix}^{T}$ in the 2D image plane. 


To express a 3D coordinate in the 2D camera plane the intrinsic $\mathbf{I}$ and extrinsic $\mathbf{E}$ matrices are used. The intrinsic matrix

\begin{equation}
    \mathbf{I}=
    \begin{bmatrix}
    f_x &   \gamma      & c_x\\
    0   &   f_y         & c_y\\
    0   &   0           & 1
    \end{bmatrix}
\end{equation}

describes the focal lengths $f_x$ in x and $f_y$ in y direction, the optical centre of the image plane in x direction $c_x$ and in y direction $c_y$ and  the skew between the axes $\gamma$.
Further the extrinsic matrix

\begin{equation}
    \mathbf{E}=
    \begin{bmatrix}
    \mathbf{R} & \mathbf{t_E}
    \end{bmatrix}
\end{equation}

consists of a rotational matrix $\mathbf{R}$ and a translation vector $\mathbf{t_E}$. Both the intrinsic and extrinsic matrix form the distortion matrix

\begin{equation}
    \mathbf{P}=\mathbf{I}\times \mathbf{E} \,.
\end{equation}

$\mathbf{P}$ describes the relation of the world coordinate system to the image coordinate system \cite{Zhang2000}. To calculate the image coordinates $m$

\begin{equation}
s \widetilde{\mathbf{m}}=\mathbf{P}\widetilde{\mathbf{M}}
\end{equation}

the augmented matrix $\mathbf{\widetilde{m}}$ for $\mathbf{m}$ and $\mathbf{\widetilde{M}}$ for $\mathbf{M}$ is used, whereas these matrices are just the homogeneous ones of the coordinates \cite{Zhang2000}\cite{website_camcalib}. Further a scaling factor $s$ is applied to the image coordinates which will be discussed later on in this section.

In this work the above mentioned matrices $\mathbf{I}$ and $\mathbf{E}$ are gained by searching chessboard corners.  For this calibration process a chessboard is integrated to the chute surface. Chessboard corners are shown in \autoref{fig:Chessboardcorners} where the algorithm is highlighting the chessboard corners with coloured dots over the length $l$ and width $w$. In Python the computer vision library \texttt{openCV} \cite{opencv_library} is used and all following function names refer to this library. The chessboard corners are found with the function \texttt{findChessboardCorners} and further the camera calibration matrices are received with the function \texttt{calibrateCamera}. Since the lens distortion of the camera does not change over the experiments the chessboard is only used once to find the distortion matrices. After the matrices are detected the chessboard plot is removed for the experiments.

\begin{figure}[H]
\centering
\includegraphics[width=1\textwidth]{3_figures/ChessboardCorners.jpg}
\caption[Detected chessboard corners]{Chessboard corners of upper chute segment marked with a circle. With the knowledge where the corners are on the chute surface, the lense of the camera can be calibrated.}
\label{fig:Chessboardcorners}
\end{figure}

With the mentioned methods, the camera distortion is corrected but there is still the need to address the perspective and scaling of each frame. To correct the perspective ArUcos marker symbols are used \cite{Garrido_Jurado_2014}. The used ArUco markers are shown in \autoref{fig:ArUcoMarker}.
\begin{figure}[H]
\centering
\includegraphics[width=1\textwidth]{3_figures/markers.jpg}
\caption{ArUco Marker}
\label{fig:ArUcoMarker}
\end{figure}
Each of the ArUcos have individual IDs giving the advantage to know which one is placed at which position on the chute. The individual IDs with its position is shown in \autoref{fig:methAruco}.
\begin{figure}[H]
\centering	
\includegraphics[width=1\linewidth]{3_figures/ArUcos}
\caption[Position and IDs of ArUco markers.]{Position and IDs of ArUco markers. The position of the ArUco markers are used to equalise the perspective of the image.}
\label{fig:methAruco}
\end{figure}
The ArUcos are created in Python with the ArUco library from openCV \cite{opencv_library}. The function used to detect the ArUcos is \texttt{detectMarkers} whereas the perspective is rectified with the knowledge of the position of the ArUcos. In the function \texttt{getPerspectiveTransform} the matrix to perform the transformation is generated. Further in \texttt{warpPerspective} the frames perspective are corrected. 
Scaling the image is again done by searching a second time the chessboard corners but in the frames which are equalized and have a new perspective. Therefore

\begin{equation}
    d_{m-x}=\frac{\sum_{n_{r}=0}^{n_{r}} \left( \frac{\sum_{n_{c}=0}^{n_{c}}\left( c_{xi}-c_{xi-1} \right)}{n_{c}}\right)}{n_{r}}
    \label{eq:meanChessCorners}
\end{equation}

calculates the mean distance between the chessboard corners for the width and length of the image. Here $d_{m-x}$ is the mean distance between the corners in x direction, whereas $n_{r}$ is the number of rows and $n_{c}$ is the number of columns of the chessboard. Since the plotted chessboards has a square size of \SI{51}{\milli\meter} the detected corners should also have \SI{51}{\pixel}. From the mean distance the scaling value is calculated and the frames are adapted with the provided \texttt{zoom} function from \texttt{scipy}.
All the above methods are applied to the calibration images  to generate the matrices and zoom values. Accordingly, the values are stored into an HDF5 file where they are applied to any video frame. A summary of the mentioned calibration steps is shown in \autoref{fig:imageCalib}.

\begin{figure}[H]
\centering
\includegraphics[width=1\textwidth]{3_figures/ImageCalib.pdf}
\caption{Camera and Image Calibration Flow chart}
\label{fig:imageCalib}
\end{figure}

\section{Optical Flow (OF)}
\label{sec:methOpticalFlow}

Several requirements are set to the optical flow algorithm which should be fulfilled. First the region of interest is the whole chute area, not only single features. Second the accuracy trumps over processing speed within the measurements. Third stability has to be adjustable within the code since different granular media is used in future experiments. Searching with the defined needs leads to the Farnebäck algorithm \cite{Farnebackfastandaccurate}. Compared to other optical flow algorithms, which are working with the gradient-based motion calculation this algorithm is working with motion estimation using orientation tensors \cite{Smiatacz2012}. The spatiotemporal information, consistent of the X and Y plane of the image with the time as third dimension, are forming the orientation tensor $\mathbf{T}$ which is for the 3D case a $3\times3$ matrix. For the estimation of $\boldsymbol{T}$ a second order polynomial
\begin{equation}
f(\mathbf{x}) \sim \mathbf{x}^{T} \mathbf{A} \mathbf{x}+\mathbf{b}^{T} \mathbf{x}+c
\end{equation}
is used. Here $\boldsymbol{A}$, $\boldsymbol{b}$ and $c$ are coefficients of the model and are calculated with Normalized Convolution \cite{Farneback.1999}. Once the model parameters are found the orientation tensor

\begin{equation}
\mathbf{T}=\mathbf{A}\mathbf{A}^T+\zeta\mathbf{b}\mathbf{b}^T
\end{equation} 

can be stated. Here $\zeta$ is a weight factor and therefore a design parameter in the function. Once $\mathbf{T}$ is defined the velocities can be calculated throughout a affine motion model like

\begin{equation}
\begin{aligned}
&v_{x}(X, Y)=a X+b Y+c \\
&v_{y}(X, Y)=d X+e Y+f
\end{aligned}
\end{equation}

whereas for the calculation of the parameters $a$, $b$, $c$, $d$, $e$ and $f$ cost functions are used \cite{Farnebackfastandaccurate}. Here some of the important techniques are mentioned from a complex algorithm out of \cite{Farnebackfastandaccurate}, but it can not describe the whole procedure which is not the aim of the work. Therefore the in depth information can be found in \cite{Farneback.1999}, \cite{Gunnar} and \cite{FarnebackConferencePaper}. \newline The algorithm is implemented in openCV \cite{opencv_library}, whereas the function \texttt{calcOpticalFlowFarneback} can be called.
The quality of the optical flow calculation depends mainly on the input parameters. In this work the main focus is set to six input parameters, namely:
\begin{itemize}
\item \texttt{pyr\_scale}: Image scale to build Pyramids  
\item \texttt{levels}: Number of pyramid layers 
\item \texttt{winsize}: Average window size 
\item \texttt{iterations}: Iterations performed at each level 
\item \texttt{poly\_n}: Pixel neighbourhood 
\item \texttt{poly\_sigma}: Standard deviation of Gaussian filter 
\end{itemize}

The mentioned declaration and description of the input parameters are taken from \cite{opencv_library}.
In the AvaChute a maximum velocity of \SI{5}{\meter\per\second} is assumed therefore the pixels jump can be a lot depending on the chosen angle of the chute. Algorithms like the one from \cite{Gunnar} are having troubles with such pixel offsets, therefore multi scale displacement estimation \cite{Gunnar}, which generates a priori knowledge of the velocity, is used. \cite{opencv_library} provides a technique named Image Pyramid to observe scaled copies of the input image for calculating the flow. With such down-sampled images, the flow can be determined easier for big movements of objects. With the outcome of the down-sampled image the algorithm is stepping one level higher and has an initial flow, where the movement ca not get lost anymore \cite{Bai2018}.  Here the scaling factor \texttt{pyr\_scale} and the levels of scaling \texttt{levels} are taken as a input of the function \texttt{calcOpticalFlowFarneback}.\newline
In addition the stability of the result can be influenced by setting the parameter of the window size \texttt{winsize}. On the one side with a growing window size the stability is increasing but the outcome is also more blurred over the whole velocity field. Here some parameters were tried and discussed later on in \autoref{tab:parameterOF}.\newline
The algorithm implemented in \cite{opencv_library} also takes \texttt{iterations} as an input parameter, which defines the maximum iterations performed per pyramid level to reach the convergence \cite{Gunnar}.\newline
By increasing the neighbourhood size \texttt{poly\_n} the velocity field gets more smoothed but in addition the algorithm will be more stable and robust, since this parameter is used to find polynomial expansion in the pixels for the Farneback algorithm \cite{Gunnar}\cite{opencv_library}. The last parameter \texttt{ploy\_sigma} defines the standard deviation for the Gaussian filter which is applied over the neighbourhood to get rid of the noise in the image.
The implemented code is shown in \autoref{sec:of}.


\section{Structured Light Illumination (SLI)}
\label{sec:methsti}

The structured illumination principle is shown in \autoref{fig:prinzipSTI}. The red line laser is placed with a defined angle $\phi$ sideways to the chute. Whenever an object passes through the projected line, the line seems diverted when seen from a different angle. The camera is perpendicular to the chute and shows the diverted laser line as an offset $\Delta s$ from the top view sketch in \autoref{fig:prinzipSTI}. 
The height $h$ 
\begin{equation}
h=\frac{\Delta s}{\tan{\phi}}
\end{equation}
of the moving object is evaluated from $\Delta s$ of each pixel where the laser beam is identified with a trigonometric relation \cite{wagh_panse_apte_2015}.

\begin{figure}[H]
\centering	
\includegraphics[width=0.45\textwidth,]{3_figures/STI.pdf}
\caption{Principle structured light illumination}
\label{fig:prinzipSTI}
\end{figure}

Four techniques, namely 
\begin{itemize}
\item Gray threshold,
\item HSV - Colorspace definition,
\item HSV - Colorspace definition with Fast-Fourier-transform (FFT) high-pass filter and
\item Canny edge detection,
\end{itemize}

are applied for the extraction and detection of the red laser line in each frames frame of the video. As the name states for the gray threshold the image is converted to grayscale image and further a threshold is set to filter the bright pixels.  Whereas HSV stands for H hue, S saturation and V for value. In contrast to the red, green and blue colorspace, HSV has the advantage that all red-like colors correspond to a certain hue and light or dark red is defined by the saturation and value. Since the red laser can only have the same red color after reflect, but very different brightness, HSV is therefore very well suited for tresholding according to color.\newline
The threshold values to extract the laser, or more precise the lower and upper threshold for hue, saturation and value that turns the image into a binary mask of the pixels that contain the laser, are found manually. These specific thresholds are mentioned in \autoref{sec:results}, because the HSV technique is also used in combination with FFT high-pass filter and some adjustments were made therefore. With these defined boundaries only red colors are recognized by the filter.\newline
High-pass filtering of an image can be interpreted as an edge detector around images because features like sharp changes contain mainly high spatial frequency. First step is to get the frequency information of the image to create a mask which filters out all low frequency parts of the image. Once the filter is applied, the image is further processed with the HSV - technique and again boundaries are set where only red colors are recognized.\newline
The last extraction technique used is the Canny edge detection developed by \cite{canny1986computational} and the implemented code version is taken from \cite{harris2020array}. In this algorithm the image gets first smoothed by applying a Gaussian filter, after that the gradients of the images are found with Sobel matrices. Then a non-maximum-suppression happens, which deletes everything except the maximums in the image. After that the two thresholds are evaluating whether the edge is noise or it is indeed a laser line. This short description is summed up from \cite{Rashmi2013}.
The implemented code of the SLI algorithm is found in \autoref{sec:code}.
\section{Data Storage}
\label{sec:methDataStorage}
All measured and calculated data are consistently saved to a HDF5-file, which is an open file format for fast and structured storing of large data sets. Especially for the camera calibration and the optical flow a more in depth storage structure is build. The basic structure for the flow calculation is shown in \autoref{fig:dataStorage}, whereas $t$ denotes the timestamp. First all input parameters and settings are saved to a parameter group. The velocity information of the optical flow algorithm is then
stored in new groups for each extracted velocity information, e.g. velocity $v_x$ in $x$ direction, velocity $v_y$ and the absolute velocity $v_{abs}$. For each timestamp the velocity information are stored in a new subgroup named after the input image. For the calibration matrices mentioned in \autoref{sec:methCalibrateImages} a similar storage scheme is applied and each of the individual matrices are stored in separate groups.
The code implementation of the data storage can be found in \autoref{sec:hdf5Storage}.
\begin{figure}[H]
\centering
\includegraphics[width=1\textwidth]{3_figures/StorageScheme.pdf}
\caption[Data storage scheme]{Growing data storage of optical flow data. Every frame of the video has a velocity field which is saved to the defined group.}
\label{fig:dataStorage}
\end{figure}

\section{Experiment Settings}
\label{sec:experiments}

The experiments with the mentioned methods are carried out with different settings regarding the lightning, the flow algorithm parameters as well as different measuring objects.
The light for example is first pulsed with green, blue and RGBW, as mentioned in \autoref{sec:setupava} where the optical flow and SLI are evaluated. Followed by a constant lightning with RGBW the same calculations are carried out again to evaluate each single method. The experiments are listed in \autoref{tab:experiments}. Note, for all pulsed experiments the pulse duration $T_L-on$ is set to \SI{210}{\milli\second} and $T_{L-off}$ is set to \SI{40}{\milli\second}. To get an idea how the camera view looks like, some video frames of Experiment nr. 5 are shown in \autoref{app:imageSequence}.

\begin{table}[h]
 \centering
 \caption[]{Light settings for different experiments (Exp.). Angle of the chute parts $\alpha$ and $\beta$ is always kept the same. Whenever Reference (Ref.) Objects are used no movement is happening only the SLI is evaluated.}
 \begin{tabular}{c|cccccc}
 \toprule
 Exp. Nr. & Light & Evaluation & Material & $\alpha$ / \SI{}{\degree} & $\beta$ / \SI{}{\degree} \\
 \midrule
1 & Pulsed Green  	& OSM 				& Granulate		& 34	& 27\\
2 & Pulsed Blue 	& OSM 				& Granulate		& 34	& 27\\
3 & Pulsed RGBW  	& OSM, OF, SLI 	& Ref. Objects	& 34 	& 27\\
4 & Pulsed RGBW  	& OSM, OF, SLI 	& Granulate		& 34 	& 27\\
5 & RGBW On			& OSM, OF			& Granulate  	& 34	& 27\\
6 & Dark (no light)	& SLI				& Granulate  	& 34	& 27\\ 	
 \bottomrule 
 \end{tabular}
 \label{tab:experiments}
 \end{table}

Measurements 3 and 4 in \autoref{tab:experiments} are carried out with the \SI{20}{\milli\watt} laser at the laser position 1 shown in \autoref{fig:avachutecomp}. Experiment nr. 6 is performed with both lasers (\SI{20}{\milli\watt} and the \SI{100}{\milli\watt}) to evaluate the difference in the resulting frames and their effect on post-processing algorithms. Therefore the \SI{100}{\milli\watt} laser is mounted at laser position 1 and the \SI{20}{\milli\watt} laser is mounted at laser position 2, projecting two parallel lines to the chute surface. They're mounted in a way that both lasers have the same angle to the plumb, marked as $\phi$ in \autoref{fig:avachutecomp}.\newline
Moreover with the video of the flow during experiment 5 (\autoref{tab:experiments}) a parameter study is performed to find the optimal settings for the optical flow algorithm. These parameters are listed in \autoref{tab:parameterOF}. The aim of the parameter study is to reduce the maximum of noise with a proper window size and increase the granulate recognition by increasing the pyramid levels.

\begin{table}[H]
 \centering
 \caption[Input parameter of optical flow algorithm]{Input parameter of optical flow algorithm}
 \begin{tabular}{c|cccccc}
 \toprule
 Parameter & Setup 1 & Setup 2 & Setup 3 & Setup 4 & Setup 5 & Setup 6\\
 \midrule
pyr\_scale   	& 0.7  	&  0.7	& 	0.7		& 0.7	& 0.7	& 0.7	\\
levels      	& 1 	&	10	& 10		& 10	& 10 	& 10		\\
winsize     	& 4  	& 	4 	& 2 		& 4		& 8 	& 16		\\
poly\_n      	& 7 	&	7	& 7			& 5		& 7 	& 7			\\
 	
 \bottomrule 
 \end{tabular}
 \label{tab:parameterOF}
 \end{table}

Some inputs are kept constant since with this the best results are achieved. The pyramid scale pyr\_scale is set to 0.7, at each pyramid, 3 iterations are performed, the standard deviation of the Gaussian filter poly\_sigma is set to 1 and no flags are used for the algorithm.



























